package edu.uprm.cse.datastructures.cardealer.util;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	//Node	`
	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element=element;
			this.next=next;
			this.previous=previous;
		}
		public Node() {
			super();
		}
		public E getElement() {
			return element;
		}
		public Node<E> getNext(){
			return next;
		}
		public Node<E> getPrevious(){
			return previous;
		}
		public void setElement(E e) {
			this.element=e;
		}
		public void setNext(Node<E> newNode)
		{
			this.next=newNode;
		}
		public void setPrevious(Node<E> prevNode) {
			this.previous=prevNode;
		}

	}
	
	//ITERATOR
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> next;
		private Node<E> previous;

		public CircularSortedDoublyLinkedListIterator() {
			this.next= (Node<E>) header.getNext();
			this.previous= (Node<E>) header.getPrevious();
		}

		@Override
		public boolean hasNext() {
			return next!=header;
		}
		public boolean hasPrevious() {
			return previous!=header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E item = this.next.getElement();
				this.next=this.next.next;
				return item;
			}
			else {
				throw new NoSuchElementException();
			}
		}

		public E previous() {
			if (this.hasPrevious()) {
				E item = this.previous.getElement();
				this.previous=this.previous.previous;
				return item;
			}
			else {
				throw new NoSuchElementException();
			}
		}


	}

	//Constructer(CSDLL)
	private int currentSize;
	private Node<E> header;
	private Node<E> previous;
	private Comparator<E> carComp;

	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.currentSize=0;
		this.header = new Node<>();
		this.carComp = comp;
	}


	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator();
	}

	@Override
	public boolean add(E e) {// adds a new elements to the list
		if (this.isEmpty()) {//If the list is empty, the next and the prev of the added node will be the header
			Node<E> temp= new Node<E>(e,header,header);
			header.setNext(temp);
			header.setPrevious(temp);
			currentSize++;
			return true;	
		}

		Node<E> newNode = new Node<E>(e, null, null);
		E obj = newNode.getElement();
		
		//goes through the list to add the element, compares it to each element on the list to maintain the list sorted 
		for(Node<E>temp=this.header.getNext(); temp!=header; temp=temp.getNext()) {
			if(this.carComp.compare(temp.getElement(), obj)>0) {
				newNode.setNext(temp);
				newNode.setPrevious(temp.getPrevious());
				temp.getPrevious().setNext(newNode);
				temp.setPrevious(newNode);
				currentSize++;
				return true;
			}
		}
		
		//Adds it at the end
		newNode.setPrevious(header.getPrevious());;
		newNode.setNext(header);
		header.setPrevious(newNode);
		newNode.getPrevious().setNext(newNode);
		currentSize++;
		return true;
	}
	

	@Override
	//Gives amounts on list
	public int size() {
		return this.currentSize;
	}

	@Override
	//Removes the first node the the given object as a element (uses remove(int index), therefore doesn't need to decrease the size of the list-)
	public boolean remove(E obj) {
		int pos = this.firstIndex(obj);
		
		if(pos<0) return false;
		else return this.remove(pos);
	}

	@Override
	//Removes the element at a given position
	public boolean remove(int index) {
		if (index<0 || index>this.size()) {
			throw new IndexOutOfBoundsException();
		}
		
		int counter=0;
		Node<E> temp = this.header.getNext();
		while(counter != index) {
				counter++;
				temp = temp.getNext();
			}
		//Removes the node, and sets the its next and prev to its adjacent nodes
		temp.getPrevious().setNext(temp.getNext());
		temp.getNext().setPrevious(temp.getPrevious());
		temp.setNext(null);
		temp.setPrevious(null);
		temp.setElement(null);
		this.currentSize--; //reduces the list size
		return true;
	}

	@Override
	//Removes all nodes that a have the given object as an element
	public int removeAll(E obj) {
		int count=0;
		while(this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	//Gives the first element in the list
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	//Gives the last element
	public E last() {
		return this.header.getPrevious().getElement();
	}

	@Override
	//Returns the element from a node on a given position
	public E get(int index) {
		if (index<0 || index> this.size()) {
			throw new IndexOutOfBoundsException();
		}
		int count=0;
		Node<E>  temp = this.header.getNext();
		while (count!=index){
			temp = temp.getNext();
			count++;
		}
		return temp.getElement();
	}

	@Override
	//Removes all elements from the the list
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}

	}

	@Override
	//If element is present return the position, else it gives a negative number
	public boolean contains(E e) {
		return this.firstIndex(e)>=0;
	}

	@Override
	//Returns true if list is empty
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	//Returns the first position of a given element, if not present, gives negative
	public int firstIndex(E e) {
		int counter=0;
		for (Node<E> temp= this.header.getNext(); temp!=header; temp = temp.getNext()) {
			if (temp.getElement().equals(e)) {
				return counter;
			}
			else {
				counter++;
			}
		}
		return -1;
	}

	@Override
	//Returns the last position of a given element
	public int lastIndex(E e) {
		int counter = this.size()-1;
		for (Node<E> temp= this.header.getPrevious(); temp!=header; temp = temp.getPrevious()) {
			if (temp.getElement().equals(e)) {
				return counter;
			}
			else {
				counter--;
			}
		}
		return -1;
	}

}

