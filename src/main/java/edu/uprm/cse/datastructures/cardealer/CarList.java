package edu.uprm.cse.datastructures.cardealer;



import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {//List where the CarManager makes the adds, deletes, updates, etc. 
	private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());

	//Added method, deletes all elements from carList
	public static void resetCars() {
		carList.clear();
	}
	
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}
}
