package edu.uprm.cse.datastructures.cardealer;

public class JsonError {
	
	private String type;
	private String message;  
	
	//Gives a message when an error occurs while updating or getting a car
	public JsonError(String type, String message){
	    this.type = type;
	    this.message = message;                
	} 
	
	public String getType(){
	    return this.type;
	}
	
	public String getMessage(){
		return this.message;
	} 
}