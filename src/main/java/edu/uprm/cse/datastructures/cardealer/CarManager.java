package edu.uprm.cse.datastructures.cardealer;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {
	private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//Return the list with all cars in it
	public Car[] getAllCars() {
		Car[] list = new Car[carList.size()];
		int i = 0;
		for(Car c: carList) {
			list[i]=c;
			i++;
		}
		return list;
	}   
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	//Returns a car with a matching id
	public Car getCar(@PathParam("id") long id){
		for(int i=0; i<carList.size();i++) {
			if (carList.get(i).getCarId()==id) {
				return carList.get(i);
			}
		}
		//If no car has a matching id
		throw new NotFoundException(new JsonError("Error", "Car" + id + " not found"));	
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCustomer(Car cars){
		for(int i=0; i<carList.size();i++) {
			if (carList.get(i).getCarId()==cars.getCarId()) {
				//If a car already has a matching id, it doesn't add ithe parameter
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
		}
		//else, it does add it
		carList.add(cars);
		return Response.status(201).build();
	}  

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	//Removes a car with same id, adds one with same id and changed values
	public Response updateCar(Car cars, @PathParam("id") int id){

		for(Car c : carList) {
			if(c.getCarId() == id) {
				carList.remove(c);
				carList.add(cars);
				return Response.status(Response.Status.OK).build();//OK=200
			}
		}
		//If no matching id found
		return Response.status(Response.Status.NOT_FOUND).build();      
	} 
	@DELETE
	@Path("/{id}/delete")
	//Removes car with matching id form the list
	public Response deleteCar(@PathParam("id") long id){
		for (Car c: carList) {
			if(c.getCarId() == id) {
				carList.remove(c);
				return Response.status(200).build();
			}
		}
		//If car is not found
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}      
}